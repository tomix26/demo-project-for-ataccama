# Demo application

Demo application for scanning data structure of target databases. For now, the only supported database is PostgreSQL, but it should be easy to add a new database connector.

## Prerequisites

- Java 8+
- Docker

## How to start

Run a postgres database in docker container:

`docker run --name demo-app-db -p 7432:5432 -e POSTGRES_USER=test -e POSTGRES_PASSWORD=test -d postgres:11-alpine`

Build and run the demo application:

`./mvnw spring-boot:run -Dspring-boot.run.arguments="--server.port=8080 --spring.datasource.url=jdbc:postgresql://localhost:7432/test --spring.datasource.username=test --spring.datasource.password=test"`

The parameters, such as the application port, database port, username or password, can be changed as desired.

When the application is started, you can add new connection details for a database that should be scanned.

Note that passwords stored in the database are kept in encrypted form. The secret phrase can be changed before the first run using `jasypt.encryptor.password` property, then the change is not possible.

Finally, you can call endpoints for scanning data structure of the target database.

## Authentication

Authentication is not implemented because it is out of the scope of this project.

## Endpoints

All endpoints consume and produce data in json format. So it is necessary to add the following http headers to each call:

```
Accept: application/json
Content-Type: application/json
```

### List databases

`GET /databases`

#### Default response

`Status: 200 OK`

```json
[
  {
    "name": "test1",
    "type": "POSTGRESQL",
    "hostname": "localhost",
    "port": 7432,
    "databaseName": "test"
  },
  {
    "name": "test2",
    "type": "POSTGRESQL",
    "hostname": "localhost",
    "port": 8432,
    "databaseName": "test"
  }
]
```

### Get a database

`GET /databases/:database`

#### Parameters

| Name       | Type   | In   | Description                 |
| ---------- | ------ | ---- | --------------------------- |
| `database` | string | path | Identifier for the database |

#### Default response

`Status: 200 OK`

```json
{
  "name": "test",
  "type": "POSTGRESQL",
  "hostname": "localhost",
  "port": 7432,
  "databaseName": "test"
}
```

### Create a database

`POST /databases`

#### Parameters

| Name           | Type    | In   | Description                         |
| -------------- | ------- | ---- | ----------------------------------- |
| `name`         | string  | body | Identifier of the database          |
| `type`         | string  | body | Type of the database                |
| `hostname`     | string  | body | Hostname to connect to the database |
| `port`         | integer | body | Hostname to connect to the database |
| `databaseName` | string  | body | Name of the database to connect     |
| `username`     | string  | body | Username to connect to the database |
| `password`     | string  | body | Password to connect to the database |

#### Sample request

```json
{
  "name": "test",
  "type": "POSTGRESQL",
  "hostname": "localhost",
  "port": "7432",
  "databaseName": "test",
  "username": "test",
  "password": "test"
}
```

#### Default response

`Status: 200 OK`

```json
{
  "name": "test",
  "type": "POSTGRESQL",
  "hostname": "localhost",
  "port": 7432,
  "databaseName": "test"
}
```

### Update a database

`PUT /databases/:database`

#### Parameters

| Name           | Type    | In   | Description                               |
| -------------- | ------- | ---- | ----------------------------------------- |
| `database`     | string  | path | Identifier of the database to be updated  |
| `name`         | string  | body | New identifier for the database           |
| `type`         | string  | body | Type of the database                      |
| `hostname`     | string  | body | Hostname to connect to the database       |
| `port`         | integer | body | Hostname to connect to the database       |
| `databaseName` | string  | body | Name of the database to connect           |
| `username`     | string  | body | Username to connect to the database       |
| `password`     | string  | body | Password to connect to the database       |

#### Sample request

```json
{
  "name": "test",
  "type": "POSTGRESQL",
  "hostname": "localhost",
  "port": "7432",
  "databaseName": "test",
  "username": "test",
  "password": "test"
}
```

#### Default response

`Status: 200 OK`

```json
{
  "name": "test",
  "type": "POSTGRESQL",
  "hostname": "localhost",
  "port": 7432,
  "databaseName": "test"
}
```

### Delete a database

`DELETE /databases/:database`

#### Parameters

| Name       | Type   | In   | Description                               |
| ---------- | ------ | ---- | ----------------------------------------- |
| `database` | string | path | Identifier for the database to be deleted |

#### Default response

`Status: 204 No Content`

### List available schemas

`GET /databases/:database/schemas`

#### Parameters

| Name       | Type   | In   | Description                 |
| ---------- | ------ | ---- | --------------------------- |
| `database` | string | path | Identifier for the database |

#### Default response

`Status: 200 OK`

```json
[
  {
    "catalogName": "test",
    "schemaName": "test1",
    "schemaOwner": "test",
    ...
  },
  {
    "catalogName": "test",
    "schemaName": "test2",
    "schemaOwner": "test",
    ...
  }
]
```

### List available tables

`GET /databases/:database/schemas/:schema/tables`

#### Parameters

| Name       | Type   | In   | Description                 |
| ---------- | ------ | ---- | --------------------------- |
| `database` | string | path | Identifier for the database |
| `schema`   | string | path | Identifier for the schema   |

#### Default response

`Status: 200 OK`

```json
[
  {
    "tableCatalog": "test",
    "tableSchema": "test",
    "tableName": "test_table",
    "tableType": "BASE TABLE",
    ...
  },
  {
    "tableCatalog": "test",
    "tableSchema": "test",
    "tableName": "test_view",
    "tableType": "VIEW",
    ...
  }
]
```

### List available columns

`GET /databases/:database/schemas/:schema/tables/:table/columns`

#### Parameters

| Name       | Type   | In   | Description                 |
| ---------- | ------ | ---- | --------------------------- |
| `database` | string | path | Identifier for the database |
| `schema`   | string | path | Identifier for the schema   |
| `table`    | string | path | Identifier for the table    |

#### Default response

`Status: 200 OK`

```json
[
  {
    "tableCatalog": "test",
    "tableSchema": "test",
    "tableName": "test_table",
    "columnName": "column1",
    "dataType": "text",
    "ordinalPosition": 1,
    "columnDefault": null,
    "isNullable": "NO",
    ...
  },
  {
    "tableCatalog": "test",
    "tableSchema": "test",
    "tableName": "test_table",
    "columnName": "column2",
    "dataType": "integer",
    "ordinalPosition": 2,
    "columnDefault": null,
    "isNullable": "NO",
    ...
  }
]
```

### Get table stats

`GET /databases/:database/schemas/:schema/tables/:table/stats`

#### Parameters

| Name       | Type   | In   | Description                 |
| ---------- | ------ | ---- | --------------------------- |
| `database` | string | path | Identifier for the database |
| `schema`   | string | path | Identifier for the schema   |
| `table`    | string | path | Identifier for the table    |

#### Default response

`Status: 200 OK`

```json
{
  "totalRecords": 10,
  "attributesCount": 5
}
```

#### Not found

`Status: 404 Not Found`

### Get column stats

`GET /databases/:database/schemas/:schema/tables/:table/columns/:column/stats`

#### Parameters

| Name       | Type   | In   | Description                 |
| ---------- | ------ | ---- | --------------------------- |
| `database` | string | path | Identifier for the database |
| `schema`   | string | path | Identifier for the schema   |
| `table`    | string | path | Identifier for the table    |
| `column`   | string | path | Identifier for the column   |

#### Default response

`Status: 200 OK`

```json
{
  "minValue": 100,
  "maxValue": 1000,
  "avgValue": 500,
  "sumValue": 50000
}
```

#### Not found

`Status: 404 Not Found`

#### Unsupported type

`Status: 422 Unprocessable Entity`

### Get table data

`GET /databases/:database/schemas/:schema/tables/:table/data`

#### Parameters

| Name       | Type   | In    | Description                         |
| ---------- | ------ | ----- | ----------------------------------- |
| `database` | string | path  | Identifier for the database         |
| `schema`   | string | path  | Identifier for the schema           |
| `table`    | string | path  | Identifier for the table            |
| `page`     | string | query | Page number of the results to fetch |
| `size`     | string | query | Results per page                    |

#### Default response

`Status: 200 OK`

```json
[
  {
    "column1": "record1",
    "column2": true,
    "column3": 12345
  },
  {
    "column1": "record2",
    "column2": false,
    "column3": 56789
  }
]
```

