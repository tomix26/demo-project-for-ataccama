package com.ataccama.vanek.scanner.postgres;

import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;
import com.google.common.collect.ImmutableMap;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.NestedTestConfiguration;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static io.zonky.test.db.AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.OVERRIDE;

@JdbcTest
@AutoConfigureEmbeddedDatabase(type = POSTGRES)
@FlywayTest(overrideLocations = true, locationsForMigrate = "flyway/postgres")
@NestedTestConfiguration(OVERRIDE)
class PostgresBasicDatabaseScannerIntegrationTest {

    @Autowired
    private DataSource dataSource;

    private PostgresBasicDatabaseScanner databaseScanner;

    @BeforeEach
    void setUp() {
        databaseScanner = new PostgresBasicDatabaseScanner(dataSource);
    }

    @Nested
    class GetSchemas {

        @Test
        void schemaList() {
            List<SchemaInfo> schemas = databaseScanner.getSchemas();
            assertThat(schemas).extracting(SchemaInfo::getSchemaName)
                    .containsExactlyInAnyOrder(
                            "test", "demo", "public",
                            "information_schema", "pg_catalog",
                            "pg_toast_temp_1", "pg_temp_1", "pg_toast");
        }

        @Test
        void schemaDetail() {
            SchemaInfo testSchema = databaseScanner.getSchemas().stream()
                    .filter(info -> StringUtils.equals(info.getSchemaName(), "test"))
                    .findFirst().orElse(null);

            assertThat(testSchema).isNotNull();
            assertThat(testSchema.getCatalogName()).isNotEmpty();
            assertThat(testSchema.getSchemaName()).isEqualTo("test");
            assertThat(testSchema.getDetails()).containsAllEntriesOf(
                    ImmutableMap.of("schemaOwner", "postgres"));
        }
    }

    @Nested
    class GetTables {

        @Test
        void tableList() {
            List<TableInfo> tables = databaseScanner.getTables("test");
            assertThat(tables).extracting(TableInfo::getTableName)
                    .containsExactlyInAnyOrder("test_table", "test_view");
        }

        @Test
        void tableDetail() {
            TableInfo testTable = databaseScanner.getTables("test").stream()
                    .filter(info -> StringUtils.equals(info.getTableName(), "test_table"))
                    .findFirst().orElse(null);

            assertThat(testTable).isNotNull();
            assertThat(testTable.getTableCatalog()).isNotEmpty();
            assertThat(testTable.getTableSchema()).isEqualTo("test");
            assertThat(testTable.getTableName()).isEqualTo("test_table");
            assertThat(testTable.getTableType()).isEqualTo("BASE TABLE");
            assertThat(testTable.getDetails()).containsAllEntriesOf(ImmutableMap.of(
                    "isInsertableInto", "YES",
                    "isTyped", "NO"));
        }

        @Test
        void viewDetail() {
            TableInfo testTable = databaseScanner.getTables("test").stream()
                    .filter(info -> StringUtils.equals(info.getTableName(), "test_view"))
                    .findFirst().orElse(null);

            assertThat(testTable).isNotNull();
            assertThat(testTable.getTableCatalog()).isNotEmpty();
            assertThat(testTable.getTableSchema()).isEqualTo("test");
            assertThat(testTable.getTableName()).isEqualTo("test_view");
            assertThat(testTable.getTableType()).isEqualTo("VIEW");
            assertThat(testTable.getDetails()).containsAllEntriesOf(ImmutableMap.of(
                    "isInsertableInto", "YES",
                    "isTyped", "NO"));
        }
    }

    @Nested
    class GetColumns {

        @Test
        void columnList() {
            List<ColumnInfo> columns = databaseScanner.getColumns("test", "test_table");
            assertThat(columns).extracting(ColumnInfo::getColumnName).containsExactlyInAnyOrder(
                    "id", "integer_value", "double_value", "date_time_value", "date_value",
                    "time_value", "boolean_value", "string_value", "varchar_value");
        }

        @Test
        void columnDetail() {
            ColumnInfo testTable = databaseScanner.getColumns("test", "test_table").stream()
                    .filter(info -> StringUtils.equals(info.getColumnName(), "string_value"))
                    .findFirst().orElse(null);

            assertThat(testTable).isNotNull();
            assertThat(testTable.getTableCatalog()).isNotEmpty();
            assertThat(testTable.getTableSchema()).isEqualTo("test");
            assertThat(testTable.getTableName()).isEqualTo("test_table");
            assertThat(testTable.getColumnName()).isEqualTo("string_value");
            assertThat(testTable.getDataType()).isEqualTo("text");
            assertThat(testTable.getDetails()).containsAllEntriesOf(ImmutableMap.of(
                    "ordinalPosition", 8,
                    "isNullable", "YES",
                    "isUpdatable", "YES",
                    "isIdentity", "NO"));
        }

        @ParameterizedTest
        @CsvSource({
                "id, bigint",
                "integer_value, integer",
                "double_value, numeric",
                "date_time_value, timestamp without time zone",
                "date_value, date",
                "time_value, time without time zone",
                "boolean_value, boolean",
                "string_value, text",
                "varchar_value, character varying"
        })
        void columnTypes(String columnName, String expectedType) {
            ColumnInfo testTable = databaseScanner.getColumns("test", "test_table").stream()
                    .filter(info -> StringUtils.equals(info.getColumnName(), columnName))
                    .findFirst().orElse(null);

            assertThat(testTable).isNotNull();
            assertThat(testTable.getDataType()).isEqualTo(expectedType);
        }
    }

    @Nested
    class FetchData {

        @Test
        void dataList() {
            List<Map<String, Object>> data = databaseScanner.fetchData("test", "test_table", 0, 10);
            assertThat(data).extracting("id").containsExactlyInAnyOrder(1L, 2L, 3L);
        }

        @Test
        void dataDetail() {
            Map<String, Object> result = databaseScanner.fetchData("test", "test_table", 0, 10).stream()
                    .filter(record -> Objects.equals(record.get("id"), 2L))
                    .findFirst().orElse(null);

            Map<String, Object> expectedResult = ImmutableMap.<String, Object>builder()
                    .put("id", 2L)
                    .put("integer_value", 222)
                    .put("double_value", new BigDecimal("2345.67"))
                    .put("date_time_value", Timestamp.valueOf("2021-03-20 18:00:00.0"))
                    .put("date_value", Date.valueOf("2021-03-20"))
                    .put("time_value", Time.valueOf("18:00:00"))
                    .put("boolean_value", false)
                    .put("string_value", "some text 2")
                    .put("varchar_value", "some text 2")
                    .build();

            assertThat(result).containsExactlyInAnyOrderEntriesOf(expectedResult);
        }

        @ParameterizedTest
        @CsvSource({
                "0, 10, 3",
                "0, 1, 1",
                "0, 2, 2",
                "0, 3, 3",
                "1, 1, 1",
                "1, 2, 2",
                "1, 3, 2",
                "2, 1, 1",
                "3, 1, 0"
        })
        void offsetAndLimit(int offset, int limit, int expectedResults) {
            List<Map<String, Object>> data = databaseScanner.fetchData("test", "test_table", offset, limit);
            assertThat(data).hasSize(expectedResults);
        }
    }
}