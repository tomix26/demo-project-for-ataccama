package com.ataccama.vanek.scanner.postgres;

import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.TableStats;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.postgresql.util.PGInterval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.NestedTestConfiguration;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Time;

import static io.zonky.test.db.AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.OVERRIDE;

@JdbcTest
@AutoConfigureEmbeddedDatabase(type = POSTGRES)
@FlywayTest(overrideLocations = true, locationsForMigrate = "flyway/postgres")
@NestedTestConfiguration(OVERRIDE)
class PostgresStatisticalDatabaseScannerIntegrationTest {

    @Autowired
    private DataSource dataSource;

    private PostgresStatisticalDatabaseScanner databaseScanner;

    @BeforeEach
    void setUp() {
        databaseScanner = new PostgresStatisticalDatabaseScanner(dataSource);
    }

    @Nested
    class GetTableStats {

        @Test
        void test() {
            TableStats tableStats = databaseScanner.getTableStats("test", "test_table");

            assertThat(tableStats).isNotNull();
            assertThat(tableStats.getTotalRecords()).isEqualTo(3);
            assertThat(tableStats.getAttributesCount()).isEqualTo(9);
        }
    }

    @Nested
    class GetColumnStats {

        @Test
        void bigintStats() {
            ColumnStats columnStats = databaseScanner.getColumnStats("test", "test_table", "id");

            assertThat(columnStats).isNotNull();
            assertThat(columnStats.getMinValue()).isEqualTo(1L);
            assertThat(columnStats.getMaxValue()).isEqualTo(3L);
            assertThat((BigDecimal) columnStats.getAvgValue()).isEqualByComparingTo(new BigDecimal(2));
            assertThat((BigDecimal) columnStats.getSumValue()).isEqualByComparingTo(new BigDecimal(6));
        }

        @Test
        void integerStats() {
            ColumnStats columnStats = databaseScanner.getColumnStats("test", "test_table", "integer_value");

            assertThat(columnStats).isNotNull();
            assertThat(columnStats.getMinValue()).isEqualTo(111);
            assertThat(columnStats.getMaxValue()).isEqualTo(333);
            assertThat((BigDecimal) columnStats.getAvgValue()).isEqualByComparingTo(new BigDecimal(222));
            assertThat(columnStats.getSumValue()).isEqualTo(666L);
        }

        @Test
        void numericStats() {
            ColumnStats columnStats = databaseScanner.getColumnStats("test", "test_table", "double_value");

            assertThat(columnStats).isNotNull();
            assertThat((BigDecimal) columnStats.getMinValue()).isEqualByComparingTo(new BigDecimal("1234.56"));
            assertThat((BigDecimal) columnStats.getMaxValue()).isEqualByComparingTo(new BigDecimal("3456.78"));
            assertThat((BigDecimal) columnStats.getAvgValue()).isEqualByComparingTo(new BigDecimal("2345.67"));
            assertThat((BigDecimal) columnStats.getSumValue()).isEqualByComparingTo(new BigDecimal("7037.01"));
        }

        @Test
        void timeStats() {
            ColumnStats columnStats = databaseScanner.getColumnStats("test", "test_table", "time_value");

            assertThat(columnStats).isNotNull();
            assertThat(columnStats.getMinValue()).isEqualTo(Time.valueOf("12:00:00"));
            assertThat(columnStats.getMaxValue()).isEqualTo(Time.valueOf("21:00:00"));
            assertThat(columnStats.getAvgValue()).isEqualTo(new PGInterval(0, 0, 0, 17, 0, 0));
            assertThat(columnStats.getSumValue()).isEqualTo(new PGInterval(0, 0, 0, 51, 0, 0));
        }

        @ParameterizedTest
        @CsvSource({
                "date_time_value",
                "date_value",
                "boolean_value",
                "string_value",
                "varchar_value"
        })
        void unsupportedTypes(String columnName) {
            assertThatCode(() -> databaseScanner.getColumnStats("test", "test_table", columnName))
                    .isExactlyInstanceOf(UnsupportedOperationException.class)
                    .hasMessageContaining(columnName);
        }
    }
}