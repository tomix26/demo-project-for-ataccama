package com.ataccama.vanek.scanner.provider;

import com.ataccama.vanek.domain.ConnectionDetails;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;

import static com.ataccama.vanek.domain.DatabaseType.POSTGRESQL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

class DataSourceProviderImplTest {

    private DataSourceProvider dataSourceProvider = new DataSourceProviderImpl();

    @Nested
    class GetDataSource {

        @Test
        void postgresType() {
            ConnectionDetails details = new ConnectionDetails(
                    "name1",
                    POSTGRESQL,
                    "hostname",
                    1234,
                    "databaseName",
                    "username",
                    "password");

            DataSource dataSource = dataSourceProvider.getDataSource(details);
            assertThat(dataSource).isExactlyInstanceOf(PGSimpleDataSource.class);

            PGSimpleDataSource postgresDataSource = (PGSimpleDataSource) dataSource;
            assertThat(postgresDataSource.getUrl()).isEqualTo("jdbc:postgresql://hostname:1234/databaseName");
            assertThat(postgresDataSource.getUser()).isEqualTo("username");
            assertThat(postgresDataSource.getPassword()).isEqualTo("password");

        }

        @Test
        void unsupportedType() {
            ConnectionDetails details = new ConnectionDetails("name2", null, null, 0, null, null, null);
            assertThatCode(() -> dataSourceProvider.getDataSource(details))
                    .isExactlyInstanceOf(UnsupportedOperationException.class);
        }
    }
}