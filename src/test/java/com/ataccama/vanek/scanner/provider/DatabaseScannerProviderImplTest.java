package com.ataccama.vanek.scanner.provider;

import com.ataccama.vanek.scanner.BasicDatabaseScanner;
import com.ataccama.vanek.scanner.StatisticalDatabaseScanner;
import com.ataccama.vanek.scanner.postgres.PostgresBasicDatabaseScanner;
import com.ataccama.vanek.scanner.postgres.PostgresStatisticalDatabaseScanner;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.mock;

class DatabaseScannerProviderImplTest {

    private DatabaseScannerProvider databaseScannerProvider = new DatabaseScannerProviderImpl();

    @Nested
    class GetBasicScanner {

        @Test
        void postgresDataSource() {
            BasicDatabaseScanner databaseScanner = databaseScannerProvider.getBasicScanner(new PGSimpleDataSource());
            assertThat(databaseScanner).isExactlyInstanceOf(PostgresBasicDatabaseScanner.class);
        }

        @Test
        void unsupportedDataSource() {
            assertThatCode(() -> databaseScannerProvider.getBasicScanner(mock(DataSource.class)))
                    .isExactlyInstanceOf(UnsupportedOperationException.class);
        }
    }

    @Nested
    class GetStatisticalScanner {

        @Test
        void postgresDataSource() {
            StatisticalDatabaseScanner databaseScanner = databaseScannerProvider.getStatisticalScanner(new PGSimpleDataSource());
            assertThat(databaseScanner).isExactlyInstanceOf(PostgresStatisticalDatabaseScanner.class);
        }

        @Test
        void unsupportedDataSource() {
            assertThatCode(() -> databaseScannerProvider.getStatisticalScanner(mock(DataSource.class)))
                    .isExactlyInstanceOf(UnsupportedOperationException.class);
        }
    }

}