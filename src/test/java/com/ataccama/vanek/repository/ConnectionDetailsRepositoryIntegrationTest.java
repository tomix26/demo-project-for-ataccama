package com.ataccama.vanek.repository;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static com.ataccama.vanek.domain.DatabaseType.POSTGRESQL;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class ConnectionDetailsRepositoryIntegrationTest {

    @Autowired
    private ConnectionDetailsRepository connectionDetailsRepository;

    @Test
    void updateRecord() {
        for (int i = 0; i < 3; i++) {
            ConnectionDetailsEntity details = new ConnectionDetailsEntity();
            details.setName("name1");
            details.setType(POSTGRESQL);
            details.setHostname("hostname");
            details.setPort(1234);
            details.setDatabaseName("databaseName");
            details.setUsername("username");
            details.setEncryptedPassword("password");

            connectionDetailsRepository.save(details);
        }

        List<ConnectionDetailsEntity> allRecords = connectionDetailsRepository.findAll();
        assertThat(allRecords).hasSize(1);
    }
}