package com.ataccama.vanek.service;

import com.ataccama.vanek.domain.ConnectionDetails;
import com.ataccama.vanek.repository.ConnectionDetailsEntity;
import com.ataccama.vanek.repository.ConnectionDetailsRepository;
import com.google.common.collect.ImmutableList;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConnectionDetailsServiceImplTest {

    @Mock
    private ConnectionDetailsRepository connectionDetailsRepository;
    @Mock
    private StringEncryptor stringEncryptor;

    @InjectMocks
    private ConnectionDetailsServiceImpl connectionDetailsService;

    @Test
    void getDatabases() {
        ConnectionDetailsEntity details = new ConnectionDetailsEntity();
        details.setName("databaseName");
        details.setEncryptedPassword("password");

        when(connectionDetailsRepository.findAll()).thenReturn(ImmutableList.of(details));
        when(stringEncryptor.decrypt(anyString())).then(returnsFirstArg());

        List<ConnectionDetails> databases = connectionDetailsService.getDatabases();
        assertThat(databases).extracting("name").containsExactlyInAnyOrder("databaseName");
    }

    @Test
    void getDatabase() {
        ConnectionDetailsEntity details = new ConnectionDetailsEntity();
        details.setName("databaseName");
        details.setEncryptedPassword("password");

        when(connectionDetailsRepository.findById("databaseName")).thenReturn(Optional.of(details));
        when(stringEncryptor.decrypt(anyString())).then(returnsFirstArg());

        Optional<ConnectionDetails> database = connectionDetailsService.getDatabase("databaseName");
        assertThat(database).hasValueSatisfying(details1 -> {
            assertThat(details1.getName()).isEqualTo("databaseName");
        });
    }

    @Test
    void saveDatabase() {
        ConnectionDetails details = new ConnectionDetails("databaseName", null, null, 0, null, null, "password");

        when(stringEncryptor.encrypt(anyString())).then(returnsFirstArg());
        when(stringEncryptor.decrypt(anyString())).then(returnsFirstArg());
        when(connectionDetailsRepository.save(any())).then(returnsFirstArg());

        connectionDetailsService.saveDatabase(details);
        verify(connectionDetailsRepository).save(argThat(entity -> entity.getName().equals("databaseName")));
    }

    @Test
    void updateDatabase() {
        ConnectionDetails details = new ConnectionDetails("databaseName", null, null, 0, null, null, "password");

        when(stringEncryptor.encrypt(anyString())).then(returnsFirstArg());
        when(stringEncryptor.decrypt(anyString())).then(returnsFirstArg());
        when(connectionDetailsRepository.save(any())).then(returnsFirstArg());

        connectionDetailsService.updateDatabase("databaseName", details);

        InOrder inOrder = inOrder(connectionDetailsRepository);
        inOrder.verify(connectionDetailsRepository).deleteById("databaseName");
        inOrder.verify(connectionDetailsRepository).save(argThat(entity -> entity.getName().equals("databaseName")));
    }

    @Test
    void deleteDatabase() {
        connectionDetailsService.deleteDatabase("databaseName");
        verify(connectionDetailsRepository).deleteById("databaseName");
    }
}