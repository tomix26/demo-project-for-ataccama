package com.ataccama.vanek.controller;

import com.ataccama.vanek.ApiUrl;
import com.ataccama.vanek.domain.ConnectionDetails;
import com.ataccama.vanek.service.ConnectionDetailsService;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static com.ataccama.vanek.domain.DatabaseType.POSTGRESQL;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ConnectionDetailsController.class)
class ConnectionDetailsControllerIntegrationTest {

    @MockBean
    private ConnectionDetailsService connectionDetailsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getDatabases() throws Exception {
        ConnectionDetails details = new ConnectionDetails(
                "name1",
                POSTGRESQL,
                "hostname",
                1234,
                "databaseName",
                "username",
                "password");

        when(connectionDetailsService.getDatabases()).thenReturn(ImmutableList.of(details));

        mockMvc.perform(get(ApiUrl.DATABASES)
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("name1"))
                .andExpect(jsonPath("$[0].type").value("POSTGRESQL"))
                .andExpect(jsonPath("$[0].hostname").value("hostname"))
                .andExpect(jsonPath("$[0].port").value(1234))
                .andExpect(jsonPath("$[0].databaseName").value("databaseName"))
                .andExpect(jsonPath("$[0].username").doesNotExist())
                .andExpect(jsonPath("$[0].password").doesNotExist());
    }

    @Test
    void getDatabase() throws Exception {
        ConnectionDetails details = new ConnectionDetails(
                "name1",
                POSTGRESQL,
                "hostname",
                1234,
                "databaseName",
                "username",
                "password");

        when(connectionDetailsService.getDatabase("databaseName")).thenReturn(Optional.of(details));

        mockMvc.perform(get(ApiUrl.DATABASES_ID, "databaseName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.type").value("POSTGRESQL"))
                .andExpect(jsonPath("$.hostname").value("hostname"))
                .andExpect(jsonPath("$.port").value(1234))
                .andExpect(jsonPath("$.databaseName").value("databaseName"))
                .andExpect(jsonPath("$.username").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist());
    }

    @Test
    void saveDatabase() throws Exception {
        ConnectionDetails details = new ConnectionDetails(
                "name1",
                POSTGRESQL,
                "hostname",
                1234,
                "databaseName",
                "username",
                "password");

        when(connectionDetailsService.saveDatabase(any())).thenReturn(details);

        mockMvc.perform(post(ApiUrl.DATABASES)
                .accept(APPLICATION_JSON)
                .contentType(APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"name1\",\n" +
                        "  \"type\": \"POSTGRESQL\",\n" +
                        "  \"hostname\": \"hostname\",\n" +
                        "  \"port\": \"1234\",\n" +
                        "  \"databaseName\": \"databaseName\",\n" +
                        "  \"username\": \"username\",\n" +
                        "  \"password\": \"password\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.type").value("POSTGRESQL"))
                .andExpect(jsonPath("$.hostname").value("hostname"))
                .andExpect(jsonPath("$.port").value(1234))
                .andExpect(jsonPath("$.databaseName").value("databaseName"))
                .andExpect(jsonPath("$.username").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist());
    }

    @Test
    void updateDatabase() throws Exception {
        ConnectionDetails details = new ConnectionDetails(
                "name1",
                POSTGRESQL,
                "hostname",
                1234,
                "databaseName",
                "username",
                "password");

        when(connectionDetailsService.updateDatabase(eq("databaseName"), any())).thenReturn(details);

        mockMvc.perform(put(ApiUrl.DATABASES_ID, "databaseName")
                .accept(APPLICATION_JSON)
                .contentType(APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"name1\",\n" +
                        "  \"type\": \"POSTGRESQL\",\n" +
                        "  \"hostname\": \"hostname\",\n" +
                        "  \"port\": \"1234\",\n" +
                        "  \"databaseName\": \"databaseName\",\n" +
                        "  \"username\": \"username\",\n" +
                        "  \"password\": \"password\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.type").value("POSTGRESQL"))
                .andExpect(jsonPath("$.hostname").value("hostname"))
                .andExpect(jsonPath("$.port").value(1234))
                .andExpect(jsonPath("$.databaseName").value("databaseName"))
                .andExpect(jsonPath("$.username").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist());
    }

    @Test
    void deleteDatabase() throws Exception {
        mockMvc.perform(delete(ApiUrl.DATABASES_ID, "databaseName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());

        verify(connectionDetailsService).deleteDatabase("databaseName");
    }
}