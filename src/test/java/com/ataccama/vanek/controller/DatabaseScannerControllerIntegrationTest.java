package com.ataccama.vanek.controller;

import com.ataccama.vanek.ApiUrl;
import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;
import com.ataccama.vanek.scanner.to.TableStats;
import com.ataccama.vanek.service.DatabaseScanningService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DatabaseScannerController.class)
class DatabaseScannerControllerIntegrationTest {

    @MockBean
    private DatabaseScanningService databaseScanningService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getSchemas() throws Exception {
        SchemaInfo schemaInfo = new SchemaInfo("catalogName", "schemaName",
                ImmutableMap.of("testAttribute", "attribute value"));

        when(databaseScanningService.getSchemas("databaseName")).thenReturn(ImmutableList.of(schemaInfo));

        mockMvc.perform(get(ApiUrl.SCHEMAS, "databaseName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].catalogName").value("catalogName"))
                .andExpect(jsonPath("$[0].schemaName").value("schemaName"))
                .andExpect(jsonPath("$[0].testAttribute").value("attribute value"));
    }

    @Test
    void getTables() throws Exception {
        TableInfo tableInfo = new TableInfo(
                "catalogName", "schemaName",
                "tableName", "tableType",
                ImmutableMap.of("testAttribute", "attribute value"));

        when(databaseScanningService.getTables("databaseName", "schemaName")).thenReturn(ImmutableList.of(tableInfo));

        mockMvc.perform(get(ApiUrl.TABLES, "databaseName", "schemaName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].tableCatalog").value("catalogName"))
                .andExpect(jsonPath("$[0].tableSchema").value("schemaName"))
                .andExpect(jsonPath("$[0].tableName").value("tableName"))
                .andExpect(jsonPath("$[0].tableType").value("tableType"))
                .andExpect(jsonPath("$[0].testAttribute").value("attribute value"));
    }

    @Test
    void getColumns() throws Exception {
        ColumnInfo columnInfo = new ColumnInfo(
                "catalogName", "schemaName",
                "tableName", "columnName", "dataType",
                ImmutableMap.of("testAttribute", "attribute value"));

        when(databaseScanningService.getColumns("databaseName", "schemaName", "tableName")).thenReturn(ImmutableList.of(columnInfo));

        mockMvc.perform(get(ApiUrl.COLUMNS, "databaseName", "schemaName", "tableName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].tableCatalog").value("catalogName"))
                .andExpect(jsonPath("$[0].tableSchema").value("schemaName"))
                .andExpect(jsonPath("$[0].tableName").value("tableName"))
                .andExpect(jsonPath("$[0].columnName").value("columnName"))
                .andExpect(jsonPath("$[0].dataType").value("dataType"))
                .andExpect(jsonPath("$[0].testAttribute").value("attribute value"));
    }

    @Test
    void getTableStats() throws Exception {
        TableStats tableStats = new TableStats(100, 10);

        when(databaseScanningService.getTableStats("databaseName", "schemaName", "tableName")).thenReturn(Optional.of(tableStats));

        mockMvc.perform(get(ApiUrl.TABLE_STATS, "databaseName", "schemaName", "tableName", "columnName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalRecords").value(100))
                .andExpect(jsonPath("$.attributesCount").value(10));
    }

    @Test
    void getColumnStats() throws Exception {
        ColumnStats columnStats = new ColumnStats(0, 1000, 500, 50000);

        when(databaseScanningService.getColumnStats("databaseName", "schemaName", "tableName", "columnName")).thenReturn(Optional.of(columnStats));

        mockMvc.perform(get(ApiUrl.COLUMN_STATS, "databaseName", "schemaName", "tableName", "columnName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.minValue").value(0))
                .andExpect(jsonPath("$.maxValue").value(1000))
                .andExpect(jsonPath("$.avgValue").value(500))
                .andExpect(jsonPath("$.sumValue").value(50000));
    }

    @Test
    void getColumnStatsUnsupportedType() throws Exception {
        when(databaseScanningService.getColumnStats("databaseName", "schemaName", "tableName", "columnName")).thenThrow(UnsupportedOperationException.class);

        mockMvc.perform(get(ApiUrl.COLUMN_STATS, "databaseName", "schemaName", "tableName", "columnName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void getTableData() throws Exception {
        List<Map<String, Object>> tableData = ImmutableList.of(ImmutableMap.of(
                "column1", "record1",
                "column2", true,
                "column3", 12345));

        when(databaseScanningService.fetchData("databaseName", "schemaName", "tableName", 0, 10)).thenReturn(tableData);

        mockMvc.perform(get(ApiUrl.TABLE_DATA, "databaseName", "schemaName", "tableName")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].column1").value("record1"))
                .andExpect(jsonPath("$[0].column2").value(true))
                .andExpect(jsonPath("$[0].column3").value(12345));
    }

    @Test
    void getTableDataWithPagination() throws Exception {
        List<Map<String, Object>> tableData = ImmutableList.of(ImmutableMap.of(
                "column1", "record1",
                "column2", true,
                "column3", 12345));

        when(databaseScanningService.fetchData("databaseName", "schemaName", "tableName", 2, 5)).thenReturn(tableData);

        mockMvc.perform(get(ApiUrl.TABLE_DATA, "databaseName", "schemaName", "tableName")
                .param("page", "2").param("size", "5")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].column1").value("record1"))
                .andExpect(jsonPath("$[0].column2").value(true))
                .andExpect(jsonPath("$[0].column3").value(12345));
    }
}