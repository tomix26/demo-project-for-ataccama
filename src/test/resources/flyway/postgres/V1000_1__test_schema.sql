create schema if not exists test;

create table test.test_table (
    id                bigint not null,
    integer_value     int,
    double_value      numeric(19, 2),
    date_time_value   timestamp,
    date_value        date,
    time_value        time,
    boolean_value     boolean,
    string_value      text,
    varchar_value     varchar(255)
);

create view test.test_view as select id from test.test_table;

insert into test.test_table (id, integer_value, double_value, date_time_value, date_value, time_value, boolean_value, string_value, varchar_value)
values (1, 111, '1234.56', '2021-03-19 12:00:00.000000', '2021-03-19', '12:00:00', true, 'some text 1', 'some text 1'),
       (2, 222, '2345.67', '2021-03-20 18:00:00.000000', '2021-03-20', '18:00:00', false, 'some text 2', 'some text 2'),
       (3, 333, '3456.78', '2021-03-21 21:00:00.000000', '2021-03-21', '21:00:00', true, 'some text 3', 'some text 3');
