package com.ataccama.vanek.repository;

import com.ataccama.vanek.domain.DatabaseType;
import com.google.common.base.MoreObjects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "demo", name = "connection_details")
public class ConnectionDetailsEntity {

    @Id
    private String name;
    @Enumerated(EnumType.STRING)
    private DatabaseType type;
    private String hostname;
    private int port;
    private String databaseName;
    private String username;
    private String encryptedPassword;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DatabaseType getType() {
        return type;
    }

    public void setType(DatabaseType type) {
        this.type = type;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String password) {
        this.encryptedPassword = password;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("type", type)
                .add("hostname", hostname)
                .add("port", port)
                .add("databaseName", databaseName)
                .add("username", username)
                .add("password", "*****")
                .toString();
    }
}
