package com.ataccama.vanek.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConnectionDetailsRepository extends JpaRepository<ConnectionDetailsEntity, String> {
}
