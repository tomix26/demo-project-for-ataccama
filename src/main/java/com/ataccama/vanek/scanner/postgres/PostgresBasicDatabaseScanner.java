package com.ataccama.vanek.scanner.postgres;

import com.ataccama.vanek.scanner.BasicDatabaseScanner;
import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ataccama.vanek.support.SqlPreconditions.checkSqlArguments;
import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;
import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRES_NEW;

public class PostgresBasicDatabaseScanner implements BasicDatabaseScanner {

    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;

    public PostgresBasicDatabaseScanner(DataSource dataSource) {
        DefaultTransactionAttribute transactionDefinition = new DefaultTransactionAttribute();
        transactionDefinition.setPropagationBehavior(PROPAGATION_REQUIRES_NEW);
        transactionDefinition.setReadOnly(true);

        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.transactionTemplate = new TransactionTemplate(new DataSourceTransactionManager(dataSource), transactionDefinition);
    }

    @Override
    public List<SchemaInfo> getSchemas() {
        return transactionTemplate.execute(status -> {
            List<Map<String, Object>> result = jdbcTemplate.queryForList("select * from information_schema.schemata");
            return result.stream().map(data -> {
                String catalog = (String) data.remove("catalog_name");
                String schema = (String) data.remove("schema_name");
                return new SchemaInfo(catalog, schema, normalizeKeys(data));
            }).collect(Collectors.toList());
        });
    }

    @Override
    public List<TableInfo> getTables(String schemaName) {
        return transactionTemplate.execute(status -> {
            List<Map<String, Object>> result = jdbcTemplate.queryForList("select * from information_schema.tables where table_schema = ?", schemaName);
            return result.stream().map(data -> {
                String catalog = (String) data.remove("table_catalog");
                String schema = (String) data.remove("table_schema");
                String table = (String) data.remove("table_name");
                String type = (String) data.remove("table_type");
                return new TableInfo(catalog, schema, table, type, normalizeKeys(data));
            }).collect(Collectors.toList());
        });
    }

    @Override
    public List<ColumnInfo> getColumns(String schemaName, String tableName) {
        return transactionTemplate.execute(status -> {
            List<Map<String, Object>> result = jdbcTemplate.queryForList("select * from information_schema.columns where table_schema = ? and table_name = ?", schemaName, tableName);
            return result.stream().map(data -> {
                String catalog = (String) data.remove("table_catalog");
                String schema = (String) data.remove("table_schema");
                String table = (String) data.remove("table_name");
                String column = (String) data.remove("column_name");
                String type = (String) data.remove("data_type");
                return new ColumnInfo(catalog, schema, table, column, type, normalizeKeys(data));
            }).collect(Collectors.toList());
        });
    }

    @Override
    public List<Map<String, Object>> fetchData(String schemaName, String tableName, int offset, int limit) {
        checkSqlArguments(schemaName, tableName);

        return transactionTemplate.execute(status ->
                jdbcTemplate.queryForList(String.format("select * from %s.%s offset ? limit ?", schemaName, tableName), offset, limit));
    }

    private static Map<String, Object> normalizeKeys(Map<String, Object> data) {
        LinkedHashMap<String, Object> result = new LinkedHashMap<>();
        data.forEach((key, value) -> {
            String normalizedKey = LOWER_UNDERSCORE.to(LOWER_CAMEL, key);
            result.put(normalizedKey, value);
        });
        return result;
    }
}
