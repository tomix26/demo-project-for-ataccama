package com.ataccama.vanek.scanner.postgres;

import com.ataccama.vanek.scanner.StatisticalDatabaseScanner;
import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.TableStats;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

import static com.ataccama.vanek.support.SqlPreconditions.checkSqlArguments;
import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRES_NEW;

public class PostgresStatisticalDatabaseScanner implements StatisticalDatabaseScanner {

    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;

    public PostgresStatisticalDatabaseScanner(DataSource dataSource) {
        DefaultTransactionAttribute transactionDefinition = new DefaultTransactionAttribute();
        transactionDefinition.setPropagationBehavior(PROPAGATION_REQUIRES_NEW);
        transactionDefinition.setReadOnly(true);

        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.transactionTemplate = new TransactionTemplate(new DataSourceTransactionManager(dataSource), transactionDefinition);
    }

    @Override
    public TableStats getTableStats(String schemaName, String tableName) {
        checkSqlArguments(schemaName, tableName);

        return transactionTemplate.execute(status ->
                jdbcTemplate.queryForObject(String.format("select count(*) as total, (select count(*) from information_schema.columns where table_schema = '%s' and table_name = '%s') as attributes from %s.%s",
                        schemaName, tableName, schemaName, tableName), (rs, rowNum) -> {
                    int totalRecords = rs.getInt("total");
                    int attributesCount = rs.getInt("attributes");
                    return new TableStats(totalRecords, attributesCount);
                }));
    }

    @Override
    public ColumnStats getColumnStats(String schemaName, String tableName, String columnName) throws UnsupportedOperationException {
        checkSqlArguments(schemaName, tableName, columnName);
        try {
            return transactionTemplate.execute(status ->
                    jdbcTemplate.queryForObject(String.format("select min(%s) as min, max(%s) as max, avg(%s) as avg, sum(%s) as sum from %s.%s",
                            columnName, columnName, columnName, columnName, schemaName, tableName), (rs, rowNum) -> {
                        Object minValue = rs.getObject("min");
                        Object maxValue = rs.getObject("max");
                        Object avgValue = rs.getObject("avg");
                        Object sumValue = rs.getObject("sum");
                        return new ColumnStats(minValue, maxValue, avgValue, sumValue);
                    }));
        } catch (BadSqlGrammarException e) {
            throw new UnsupportedOperationException(String.format(
                    "Statistical information is not supported for the specified column [%s]", columnName));
        }
    }
}
