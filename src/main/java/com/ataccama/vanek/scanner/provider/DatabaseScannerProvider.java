package com.ataccama.vanek.scanner.provider;

import com.ataccama.vanek.scanner.BasicDatabaseScanner;
import com.ataccama.vanek.scanner.StatisticalDatabaseScanner;

import javax.sql.DataSource;

public interface DatabaseScannerProvider {

    BasicDatabaseScanner getBasicScanner(DataSource dataSource);

    StatisticalDatabaseScanner getStatisticalScanner(DataSource dataSource);

}
