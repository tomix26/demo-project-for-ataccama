package com.ataccama.vanek.scanner.provider;

import com.ataccama.vanek.domain.ConnectionDetails;
import com.ataccama.vanek.domain.DatabaseType;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DataSourceProviderImpl implements DataSourceProvider {

    @Override
    public DataSource getDataSource(ConnectionDetails details) {
        if (details.getType() != DatabaseType.POSTGRESQL) {
            throw new UnsupportedOperationException(String.format("Unsupported database type [%s]", details.getType()));
        }

        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[] {details.getHostname()});
        dataSource.setPortNumbers(new int[] {details.getPort()});
        dataSource.setDatabaseName(details.getDatabaseName());
        dataSource.setUser(details.getUsername());
        dataSource.setPassword(details.getPassword());

        return dataSource;
    }
}
