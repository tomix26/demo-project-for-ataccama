package com.ataccama.vanek.scanner.provider;

import com.ataccama.vanek.domain.ConnectionDetails;

import javax.sql.DataSource;

public interface DataSourceProvider {

    DataSource getDataSource(ConnectionDetails details);

}
