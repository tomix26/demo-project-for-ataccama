package com.ataccama.vanek.scanner.provider;

import com.ataccama.vanek.scanner.BasicDatabaseScanner;
import com.ataccama.vanek.scanner.StatisticalDatabaseScanner;
import com.ataccama.vanek.scanner.postgres.PostgresBasicDatabaseScanner;
import com.ataccama.vanek.scanner.postgres.PostgresStatisticalDatabaseScanner;
import org.postgresql.ds.common.BaseDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

@Component
public class DatabaseScannerProviderImpl implements DatabaseScannerProvider {

    @Override
    public BasicDatabaseScanner getBasicScanner(DataSource dataSource) {
        if (isPostgresDataSource(dataSource)) {
            return new PostgresBasicDatabaseScanner(dataSource);
        }
        throw unsupportedDataSourceType(dataSource);
    }

    @Override
    public StatisticalDatabaseScanner getStatisticalScanner(DataSource dataSource) {
        if (isPostgresDataSource(dataSource)) {
            return new PostgresStatisticalDatabaseScanner(dataSource);
        }
        throw unsupportedDataSourceType(dataSource);
    }

    protected boolean isPostgresDataSource(DataSource dataSource) {
        try {
            if (dataSource.isWrapperFor(BaseDataSource.class)) {
                return true;
            }
        } catch (SQLException ex) {
            throw new IllegalStateException("Unexpected error when preparing a database scanner", ex);
        }
        return false;
    }

    protected UnsupportedOperationException unsupportedDataSourceType(DataSource dataSource) {
        throw new UnsupportedOperationException(String.format("Unsupported type of the data source [%s]", dataSource.getClass()));
    }
}
