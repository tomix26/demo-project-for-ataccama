package com.ataccama.vanek.scanner;

import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.TableStats;

public interface StatisticalDatabaseScanner {

    TableStats getTableStats(String schemaName, String tableName);

    ColumnStats getColumnStats(String schemaName, String tableName, String columnName) throws UnsupportedOperationException;

}
