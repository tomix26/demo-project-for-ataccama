package com.ataccama.vanek.scanner.to;

import com.google.common.base.MoreObjects;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class SchemaInfo {

    private final String catalogName;
    private final String schemaName;
    private final Map<String, Object> details;

    public SchemaInfo(String catalogName, String schemaName, Map<String, Object> details) {
        this.catalogName = catalogName;
        this.schemaName = schemaName;
        this.details = new LinkedHashMap<>(details);
    }

    public String getCatalogName() {
        return catalogName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public Map<String, Object> getDetails() {
        return Collections.unmodifiableMap(details);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("catalogName", catalogName)
                .add("schemaName", schemaName)
                .toString();
    }
}
