package com.ataccama.vanek.scanner.to;

import com.google.common.base.MoreObjects;

public class ColumnStats {

    private final Object minValue;
    private final Object maxValue;
    private final Object avgValue;
    private final Object sumValue;

    public ColumnStats(Object minValue, Object maxValue, Object avgValue, Object sumValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.avgValue = avgValue;
        this.sumValue = sumValue;
    }

    public Object getMinValue() {
        return minValue;
    }

    public Object getMaxValue() {
        return maxValue;
    }

    public Object getAvgValue() {
        return avgValue;
    }

    public Object getSumValue() {
        return sumValue;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("minValue", minValue)
                .add("maxValue", maxValue)
                .add("avgValue", avgValue)
                .add("sumValue", sumValue)
                .toString();
    }
}
