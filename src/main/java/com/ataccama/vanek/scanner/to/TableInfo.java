package com.ataccama.vanek.scanner.to;

import com.google.common.base.MoreObjects;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class TableInfo {

    private final String tableCatalog;
    private final String tableSchema;
    private final String tableName;
    private final String tableType;
    private final Map<String, Object> details;

    public TableInfo(String tableCatalog, String tableSchema, String tableName, String tableType, Map<String, Object> details) {
        this.tableCatalog = tableCatalog;
        this.tableSchema = tableSchema;
        this.tableName = tableName;
        this.tableType = tableType;
        this.details = new LinkedHashMap<>(details);
    }

    public String getTableCatalog() {
        return tableCatalog;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableType() {
        return tableType;
    }

    public Map<String, Object> getDetails() {
        return Collections.unmodifiableMap(details);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tableCatalog", tableCatalog)
                .add("tableSchema", tableSchema)
                .add("tableName", tableName)
                .add("tableType", tableType)
                .toString();
    }
}
