package com.ataccama.vanek.scanner.to;

import com.google.common.base.MoreObjects;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class ColumnInfo {

    private final String tableCatalog;
    private final String tableSchema;
    private final String tableName;
    private final String columnName;
    private final String dataType;
    private final Map<String, Object> details;

    public ColumnInfo(String tableCatalog, String tableSchema, String tableName, String columnName, String dataType, Map<String, Object> details) {
        this.tableCatalog = tableCatalog;
        this.tableSchema = tableSchema;
        this.tableName = tableName;
        this.columnName = columnName;
        this.dataType = dataType;
        this.details = new LinkedHashMap<>(details);
    }

    public String getTableCatalog() {
        return tableCatalog;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDataType() {
        return dataType;
    }

    public Map<String, Object> getDetails() {
        return Collections.unmodifiableMap(details);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tableCatalog", tableCatalog)
                .add("tableSchema", tableSchema)
                .add("tableName", tableName)
                .add("columnName", columnName)
                .add("dataType", dataType)
                .toString();
    }
}
