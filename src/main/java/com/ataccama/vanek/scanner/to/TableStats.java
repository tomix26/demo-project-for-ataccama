package com.ataccama.vanek.scanner.to;

import com.google.common.base.MoreObjects;

public class TableStats {

    private final int totalRecords;
    private final int attributesCount;

    public TableStats(int totalRecords, int attributesCount) {
        this.totalRecords = totalRecords;
        this.attributesCount = attributesCount;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public int getAttributesCount() {
        return attributesCount;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalRecords", totalRecords)
                .add("attributesCount", attributesCount)
                .toString();
    }
}
