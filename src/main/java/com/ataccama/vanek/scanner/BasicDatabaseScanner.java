package com.ataccama.vanek.scanner;

import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;

import java.util.List;
import java.util.Map;

public interface BasicDatabaseScanner {

    List<SchemaInfo> getSchemas();

    List<TableInfo> getTables(String schemaName);

    List<ColumnInfo> getColumns(String schemaName, String tableName);

    List<Map<String, Object>> fetchData(String schemaName, String tableName, int offset, int limit);

}
