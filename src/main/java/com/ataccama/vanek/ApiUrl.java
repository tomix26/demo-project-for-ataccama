package com.ataccama.vanek;

public interface ApiUrl {

    String DATABASES = "/databases";
    String DATABASES_ID = "/databases/{databaseName}";
    String SCHEMAS = "/databases/{databaseName}/schemas";
    String TABLES = "/databases/{databaseName}/schemas/{schemaName}/tables";
    String COLUMNS = "/databases/{databaseName}/schemas/{schemaName}/tables/{tableName}/columns";
    String TABLE_STATS = "/databases/{databaseName}/schemas/{schemaName}/tables/{tableName}/stats";
    String COLUMN_STATS = "/databases/{databaseName}/schemas/{schemaName}/tables/{tableName}/columns/{columnName}/stats";
    String TABLE_DATA = "/databases/{databaseName}/schemas/{schemaName}/tables/{tableName}/data";

}
