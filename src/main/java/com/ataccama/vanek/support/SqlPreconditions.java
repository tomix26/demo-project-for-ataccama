package com.ataccama.vanek.support;

import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;

public class SqlPreconditions {

    private static final Pattern PARAMETER_PATTERN = Pattern.compile("^[_a-zA-Z0-9]+$");

    public static void checkSqlArgument(String parameter) {
        checkArgument(PARAMETER_PATTERN.matcher(parameter).matches(),
                "Parameter contains invalid some characters [%s]", parameter);
    }

    public static void checkSqlArguments(String... parameters) {
        for (String parameter : parameters) {
            checkSqlArgument(parameter);
        }
    }
}
