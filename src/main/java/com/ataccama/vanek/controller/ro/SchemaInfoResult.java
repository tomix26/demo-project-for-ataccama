package com.ataccama.vanek.controller.ro;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import java.util.Map;

public class SchemaInfoResult {

    private final String catalogName;
    private final String schemaName;
    private final Map<String, Object> details;

    public SchemaInfoResult(String catalogName, String schemaName, Map<String, Object> details) {
        this.catalogName = catalogName;
        this.schemaName = schemaName;
        this.details = details;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    @JsonAnyGetter
    public Map<String, Object> getDetails() {
        return details;
    }
}
