package com.ataccama.vanek.controller.ro;

import com.ataccama.vanek.domain.DatabaseType;

public class ConnectionDetailsResult {

    private final String name;
    private final DatabaseType type;
    private final String hostname;
    private final int port;
    private final String databaseName;

    public ConnectionDetailsResult(String name, DatabaseType type, String hostname, int port, String databaseName) {
        this.name = name;
        this.type = type;
        this.hostname = hostname;
        this.port = port;
        this.databaseName = databaseName;
    }

    public String getName() {
        return name;
    }

    public DatabaseType getType() {
        return type;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public String getDatabaseName() {
        return databaseName;
    }
}
