package com.ataccama.vanek.controller.ro;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import java.util.Map;

public class ColumnInfoResult {

    private final String tableCatalog;
    private final String tableSchema;
    private final String tableName;
    private final String columnName;
    private final String dataType;
    private final Map<String, Object> details;

    public ColumnInfoResult(String tableCatalog, String tableSchema, String tableName, String columnName, String dataType, Map<String, Object> details) {
        this.tableCatalog = tableCatalog;
        this.tableSchema = tableSchema;
        this.tableName = tableName;
        this.columnName = columnName;
        this.dataType = dataType;
        this.details = details;
    }

    public String getTableCatalog() {
        return tableCatalog;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDataType() {
        return dataType;
    }

    @JsonAnyGetter
    public Map<String, Object> getDetails() {
        return details;
    }
}
