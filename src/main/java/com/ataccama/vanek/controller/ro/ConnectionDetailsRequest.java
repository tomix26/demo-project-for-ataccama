package com.ataccama.vanek.controller.ro;

import com.ataccama.vanek.domain.DatabaseType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ConnectionDetailsRequest {

    @NotBlank(message = "Database identifier cannot be blank")
    private final String name;

    @NotNull(message = "Database type cannot be null")
    private final DatabaseType type;

    @NotBlank(message = "Hostname cannot be blank")
    private final String hostname;

    @NotNull(message = "Port cannot be null")
    @Min(value = 0, message = "Port must be in range 0 - 65535")
    @Max(value = 65535, message = "Port must be in range 0 - 65535")
    private final int port;

    @NotBlank(message = "Database name cannot be blank")
    private final String databaseName;

    @NotNull(message = "Username cannot be null")
    private final String username;

    @NotNull(message = "Password cannot be null")
    private final String password;

    public ConnectionDetailsRequest(String name, DatabaseType type, String hostname, int port, String databaseName, String username, String password) {
        this.name = name;
        this.type = type;
        this.hostname = hostname;
        this.port = port;
        this.databaseName = databaseName;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public DatabaseType getType() {
        return type;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
