package com.ataccama.vanek.controller.ro;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import java.util.Map;

public class TableInfoResult {

    private final String tableCatalog;
    private final String tableSchema;
    private final String tableName;
    private final String tableType;
    private final Map<String, Object> details;

    public TableInfoResult(String tableCatalog, String tableSchema, String tableName, String tableType, Map<String, Object> details) {
        this.tableCatalog = tableCatalog;
        this.tableSchema = tableSchema;
        this.tableName = tableName;
        this.tableType = tableType;
        this.details = details;
    }

    public String getTableCatalog() {
        return tableCatalog;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableType() {
        return tableType;
    }

    @JsonAnyGetter
    public Map<String, Object> getDetails() {
        return details;
    }
}
