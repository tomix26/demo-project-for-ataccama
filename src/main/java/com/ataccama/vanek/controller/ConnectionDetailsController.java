package com.ataccama.vanek.controller;

import com.ataccama.vanek.ApiUrl;
import com.ataccama.vanek.controller.ro.ConnectionDetailsRequest;
import com.ataccama.vanek.controller.ro.ConnectionDetailsResult;
import com.ataccama.vanek.domain.ConnectionDetails;
import com.ataccama.vanek.service.ConnectionDetailsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE)
public class ConnectionDetailsController {

    private final ConnectionDetailsService connectionDetailsService;

    public ConnectionDetailsController(ConnectionDetailsService connectionDetailsService) {
        this.connectionDetailsService = connectionDetailsService;
    }

    @GetMapping(path = ApiUrl.DATABASES)
    public ResponseEntity<List<ConnectionDetailsResult>> getDatabases() {
        List<ConnectionDetails> databases = connectionDetailsService.getDatabases();
        List<ConnectionDetailsResult> result = databases.stream()
                .map(this::mapConnectionDetails)
                .collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = ApiUrl.DATABASES_ID)
    public ResponseEntity<ConnectionDetailsResult> getDatabase(@PathVariable("databaseName") String databaseName) {
        ConnectionDetails result = connectionDetailsService.getDatabase(databaseName).orElse(null);
        if (result == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(mapConnectionDetails(result));
    }

    @PostMapping(path = ApiUrl.DATABASES)
    public ResponseEntity<ConnectionDetailsResult> saveDatabase(@Valid @RequestBody ConnectionDetailsRequest request) {
        ConnectionDetails details = new ConnectionDetails(
                request.getName(),
                request.getType(),
                request.getHostname(),
                request.getPort(),
                request.getDatabaseName(),
                request.getUsername(),
                request.getPassword());
        ConnectionDetails result = connectionDetailsService.saveDatabase(details);
        return ResponseEntity.ok(mapConnectionDetails(result));
    }

    @PutMapping(path = ApiUrl.DATABASES_ID)
    public ResponseEntity<ConnectionDetailsResult> updateDatabase(@PathVariable("databaseName") String databaseName, @Valid @RequestBody ConnectionDetailsRequest request) {
        ConnectionDetails details = new ConnectionDetails(
                request.getName(),
                request.getType(),
                request.getHostname(),
                request.getPort(),
                request.getDatabaseName(),
                request.getUsername(),
                request.getPassword());
        ConnectionDetails result = connectionDetailsService.updateDatabase(databaseName, details);
        return ResponseEntity.ok(mapConnectionDetails(result));
    }

    @DeleteMapping(path = ApiUrl.DATABASES_ID)
    public ResponseEntity<Void> deleteDatabase(@PathVariable("databaseName") String databaseName) {
        connectionDetailsService.deleteDatabase(databaseName);
        return ResponseEntity.noContent().build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    private ConnectionDetailsResult mapConnectionDetails(ConnectionDetails details) {
        return new ConnectionDetailsResult(
                details.getName(), details.getType(),
                details.getHostname(), details.getPort(),
                details.getDatabaseName());
    }
}
