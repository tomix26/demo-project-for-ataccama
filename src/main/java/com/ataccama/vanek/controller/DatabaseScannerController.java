package com.ataccama.vanek.controller;

import com.ataccama.vanek.ApiUrl;
import com.ataccama.vanek.controller.ro.ColumnInfoResult;
import com.ataccama.vanek.controller.ro.SchemaInfoResult;
import com.ataccama.vanek.controller.ro.TableInfoResult;
import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;
import com.ataccama.vanek.scanner.to.TableStats;
import com.ataccama.vanek.service.DatabaseScanningService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE)
public class DatabaseScannerController {

    private final DatabaseScanningService databaseScanningService;

    public DatabaseScannerController(DatabaseScanningService databaseScanningService) {
        this.databaseScanningService = databaseScanningService;
    }

    @GetMapping(path = ApiUrl.SCHEMAS)
    public ResponseEntity<List<SchemaInfoResult>> getSchemas(@PathVariable("databaseName") String databaseName) {
        List<SchemaInfo> schemas = databaseScanningService.getSchemas(databaseName);
        List<SchemaInfoResult> result = schemas.stream()
                .map(info -> new SchemaInfoResult(info.getCatalogName(), info.getSchemaName(), info.getDetails()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = ApiUrl.TABLES)
    public ResponseEntity<List<TableInfoResult>> getTables(
            @PathVariable("databaseName") String databaseName,
            @PathVariable("schemaName") String schemaName) {
        List<TableInfo> tables = databaseScanningService.getTables(databaseName, schemaName);
        List<TableInfoResult> result = tables.stream()
                .map(info -> new TableInfoResult(info.getTableCatalog(), info.getTableSchema(), info.getTableName(), info.getTableType(), info.getDetails()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = ApiUrl.COLUMNS)
    public ResponseEntity<List<ColumnInfoResult>> getColumns(
            @PathVariable("databaseName") String databaseName,
            @PathVariable("schemaName") String schemaName,
            @PathVariable("tableName") String tableName) {
        List<ColumnInfo> columns = databaseScanningService.getColumns(databaseName, schemaName, tableName);
        List<ColumnInfoResult> result = columns.stream()
                .map(info -> new ColumnInfoResult(info.getTableCatalog(), info.getTableSchema(), info.getTableName(), info.getColumnName(), info.getDataType(), info.getDetails()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = ApiUrl.TABLE_STATS)
    public ResponseEntity<TableStats> getTableStats(
            @PathVariable("databaseName") String databaseName,
            @PathVariable("schemaName") String schemaName,
            @PathVariable("tableName") String tableName) {
        TableStats tableStats = databaseScanningService.getTableStats(databaseName, schemaName, tableName).orElse(null);
        if (tableStats == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(tableStats);
    }

    @GetMapping(path = ApiUrl.COLUMN_STATS)
    public ResponseEntity<?> getTableStats(
            @PathVariable("databaseName") String databaseName,
            @PathVariable("schemaName") String schemaName,
            @PathVariable("tableName") String tableName,
            @PathVariable("columnName") String columnName) {
        ColumnStats columnStats = databaseScanningService.getColumnStats(databaseName, schemaName, tableName, columnName).orElse(null);
        if (columnStats == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(columnStats);
    }

    @GetMapping(path = ApiUrl.TABLE_DATA)
    public ResponseEntity<List<Map<String, Object>>> getTableData(
            @PathVariable("databaseName") String databaseName,
            @PathVariable("schemaName") String schemaName,
            @PathVariable("tableName") String tableName,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        List<Map<String, Object>> tableData = databaseScanningService.fetchData(databaseName, schemaName, tableName, page, size);
        return ResponseEntity.ok(tableData);
    }

    @ExceptionHandler(UnsupportedOperationException.class)
    public ResponseEntity<?> handleLogoutRequested(UnsupportedOperationException e) {
        return ResponseEntity.unprocessableEntity().build();
    }
}
