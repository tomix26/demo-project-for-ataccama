package com.ataccama.vanek.service;

import com.ataccama.vanek.domain.ConnectionDetails;

import java.util.List;
import java.util.Optional;

public interface ConnectionDetailsService {

    List<ConnectionDetails> getDatabases();

    Optional<ConnectionDetails> getDatabase(String databaseName);

    ConnectionDetails saveDatabase(ConnectionDetails details);

    ConnectionDetails updateDatabase(String databaseName, ConnectionDetails details);

    void deleteDatabase(String databaseName);

}
