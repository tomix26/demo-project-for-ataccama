package com.ataccama.vanek.service;

import com.ataccama.vanek.domain.ConnectionDetails;
import com.ataccama.vanek.repository.ConnectionDetailsEntity;
import com.ataccama.vanek.repository.ConnectionDetailsRepository;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ConnectionDetailsServiceImpl implements ConnectionDetailsService {

    private final ConnectionDetailsRepository connectionDetailsRepository;
    private final StringEncryptor stringEncryptor;

    public ConnectionDetailsServiceImpl(ConnectionDetailsRepository connectionDetailsRepository, StringEncryptor stringEncryptor) {
        this.connectionDetailsRepository = connectionDetailsRepository;
        this.stringEncryptor = stringEncryptor;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ConnectionDetails> getDatabases() {
        return connectionDetailsRepository.findAll().stream()
                .map(this::mapFromEntity)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ConnectionDetails> getDatabase(String databaseName) {
        return connectionDetailsRepository.findById(databaseName)
                .map(this::mapFromEntity);
    }

    @Override
    public ConnectionDetails saveDatabase(ConnectionDetails details) {
        return mapFromEntity(connectionDetailsRepository.save(mapToEntity(details)));
    }

    @Override
    public ConnectionDetails updateDatabase(String databaseName, ConnectionDetails details) {
        connectionDetailsRepository.deleteById(databaseName);
        return mapFromEntity(connectionDetailsRepository.save(mapToEntity(details)));
    }

    @Override
    public void deleteDatabase(String databaseName) {
        connectionDetailsRepository.deleteById(databaseName);
    }

    private ConnectionDetailsEntity mapToEntity(ConnectionDetails details) {
        ConnectionDetailsEntity entity = new ConnectionDetailsEntity();
        entity.setName(details.getName());
        entity.setType(details.getType());
        entity.setHostname(details.getHostname());
        entity.setPort(details.getPort());
        entity.setDatabaseName(details.getDatabaseName());
        entity.setUsername(details.getUsername());
        entity.setEncryptedPassword(stringEncryptor.encrypt(details.getPassword()));
        return entity;
    }

    private ConnectionDetails mapFromEntity(ConnectionDetailsEntity entity) {
        return new ConnectionDetails(
                entity.getName(),
                entity.getType(),
                entity.getHostname(),
                entity.getPort(),
                entity.getDatabaseName(),
                entity.getUsername(),
                stringEncryptor.decrypt(entity.getEncryptedPassword()));
    }
}
