package com.ataccama.vanek.service;

import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;
import com.ataccama.vanek.scanner.to.TableStats;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DatabaseScanningService {

    List<SchemaInfo> getSchemas(String databaseName);

    List<TableInfo> getTables(String databaseName, String schemaName);

    List<ColumnInfo> getColumns(String databaseName, String schemaName, String tableName);

    List<Map<String, Object>> fetchData(String databaseName, String schemaName, String tableName, int page, int size);

    Optional<TableStats> getTableStats(String databaseName, String schemaName, String tableName);

    Optional<ColumnStats> getColumnStats(String databaseName, String schemaName, String tableName, String columnName) throws UnsupportedOperationException;

}
