package com.ataccama.vanek.service;

import com.ataccama.vanek.scanner.BasicDatabaseScanner;
import com.ataccama.vanek.scanner.StatisticalDatabaseScanner;
import com.ataccama.vanek.scanner.to.ColumnInfo;
import com.ataccama.vanek.scanner.to.ColumnStats;
import com.ataccama.vanek.scanner.to.SchemaInfo;
import com.ataccama.vanek.scanner.to.TableInfo;
import com.ataccama.vanek.scanner.to.TableStats;
import com.ataccama.vanek.scanner.provider.DataSourceProvider;
import com.ataccama.vanek.scanner.provider.DatabaseScannerProvider;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Collections.emptyList;

@Service
public class DatabaseScanningServiceImpl implements DatabaseScanningService {

    private final ConnectionDetailsService connectionDetailsService;
    private final DatabaseScannerProvider databaseOperationsProvider;
    private final DataSourceProvider dataSourceProvider;

    public DatabaseScanningServiceImpl(ConnectionDetailsService connectionDetailsService, DatabaseScannerProvider databaseOperationsProvider, DataSourceProvider dataSourceProvider) {
        this.connectionDetailsService = connectionDetailsService;
        this.databaseOperationsProvider = databaseOperationsProvider;
        this.dataSourceProvider = dataSourceProvider;
    }

    @Override
    public List<SchemaInfo> getSchemas(String databaseName) {
        return execute(databaseName, BasicDatabaseScanner::getSchemas).orElse(emptyList());
    }

    @Override
    public List<TableInfo> getTables(String databaseName, String schemaName) {
        return execute(databaseName, databaseScanner -> databaseScanner.getTables(schemaName)).orElse(emptyList());
    }

    @Override
    public List<ColumnInfo> getColumns(String databaseName, String schemaName, String tableName) {
        return execute(databaseName, databaseScanner -> databaseScanner.getColumns(schemaName, tableName)).orElse(emptyList());
    }

    @Override
    public List<Map<String, Object>> fetchData(String databaseName, String schemaName, String tableName, int page, int size) {
        int offset = page * size;
        return execute(databaseName, databaseScanner -> databaseScanner.fetchData(schemaName, tableName, offset, size)).orElse(emptyList());
    }

    @Override
    public Optional<TableStats> getTableStats(String databaseName, String schemaName, String tableName) {
        return executeStatistical(databaseName, databaseScanner -> databaseScanner.getTableStats(schemaName, tableName));
    }

    @Override
    public Optional<ColumnStats> getColumnStats(String databaseName, String schemaName, String tableName, String columnName) throws UnsupportedOperationException {
        return executeStatistical(databaseName, databaseScanner -> databaseScanner.getColumnStats(schemaName, tableName, columnName));
    }

    private <T> Optional<T> execute(String databaseName, Function<BasicDatabaseScanner, T> action) {
        return connectionDetailsService.getDatabase(databaseName).map(details -> {
            DataSource dataSource = dataSourceProvider.getDataSource(details);
            BasicDatabaseScanner basicScanner = databaseOperationsProvider.getBasicScanner(dataSource);
            return action.apply(basicScanner);
        });
    }

    private <T> Optional<T> executeStatistical(String databaseName, Function<StatisticalDatabaseScanner, T> action) {
        return connectionDetailsService.getDatabase(databaseName).map(details -> {
            DataSource dataSource = dataSourceProvider.getDataSource(details);
            StatisticalDatabaseScanner statisticalScanner = databaseOperationsProvider.getStatisticalScanner(dataSource);
            return action.apply(statisticalScanner);
        });
    }
}
