package com.ataccama.vanek.domain;

public class ConnectionDetails {

    private final String name;
    private final DatabaseType type;
    private final String hostname;
    private final int port;
    private final String databaseName;
    private final String username;
    private final String password;

    public ConnectionDetails(String name, DatabaseType type, String hostname, int port, String databaseName, String username, String password) {
        this.name = name;
        this.type = type;
        this.hostname = hostname;
        this.port = port;
        this.databaseName = databaseName;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public DatabaseType getType() {
        return type;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
