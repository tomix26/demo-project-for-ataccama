create schema if not exists demo;

create table demo.connection_details (
    name               text not null primary key,
    type               text not null,
    hostname           text not null,
    port               int  not null,
    database_name      text not null,
    username           text not null,
    encrypted_password text not null,

    check (type in ('POSTGRESQL')),
    check (port between 0 and 65535)
);